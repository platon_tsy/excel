import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        ExcelPOIHelper helper = new ExcelPOIHelper();
        helper.writeExcel();


        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        String fileLocation = path.substring(0, path.length() - 1) + "temp.xlsx";

        System.out.println(helper.readExcel(fileLocation));
    }

}
